﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NuGet;
using NuGetEx.Core.Exceptions;
using NUnit.Framework;

namespace NuGetEx.Core.Tests
{
    [TestFixture]
    public partial class PackageTreeResolverTests
    {
        [Test]
        public void Resolve_GivenNullParamter_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new TestPackageTreeResolver().Resolve(null));
        }

        [Test]
        [Ignore]
        public void Thing()
        {
            var framework = new FrameworkName(".NETFramework,Version=v4.5");
            var repo = NuGet.PackageRepositoryFactory.Default.CreateRepository("https://www.nuget.org/api/v2/");
            var package = NuGet.PackageRepositoryHelper.ResolvePackage(repo, repo, null, "Microsoft.Owin.Host.SystemWeb", new NuGet.SemanticVersion("2.1.0"), false);
            //var packages = repo.GetPackages().Where(x => x.Id == "Microsoft.Owin.Host.SystemWeb" && x.Version.ToString() == "2.1.0");
            //var package = packages.First();
            // Either the dependency set that matches the target framework, or the first one that has no target framework, or there are no dependencies :)
            foreach (var d in package.DependencySets.First().Dependencies)
            {
                var a = d.Id;
            }
        }

    }
}
