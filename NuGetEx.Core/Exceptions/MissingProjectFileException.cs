﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuGetEx.Core.Exceptions
{
    public class MissingProjectFileException : InvalidOperationException
    {
        public MissingProjectFileException(string path)
            : base(string.Format("No project (csproj) could be found at {0}.", path))
        {
        }
    }
}
