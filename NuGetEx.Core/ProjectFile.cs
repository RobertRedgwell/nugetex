﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using NuGetEx.Core.Exceptions;

namespace NuGetEx.Core
{
    public class ProjectFile : IProjectFile
    {
        private const string NamespacePrefix = "ms";

        public ProjectFile(FileInfoBase projectFile)
        {
            if (projectFile == null) throw new ArgumentNullException("projectFile");
            if (!projectFile.Exists) throw new MissingProjectFileException(projectFile.FullName);

            ProjectPath = projectFile.FullName;
            References = new List<IProjectFile>();

            var xml = new XmlDocument();
            try
            {
                using (var stream = projectFile.OpenText())
                {
                    xml.Load(stream);

                    XmlNamespaceManager nsmgr = new XmlNamespaceManager(xml.NameTable);
                    nsmgr.AddNamespace(NamespacePrefix, "http://schemas.microsoft.com/developer/msbuild/2003");

                    ProjectName = GetNodeValueFromPropertyGroup(xml, nsmgr, "AssemblyName");
                    var framework = GetNodeValueFromPropertyGroup(xml, nsmgr, "TargetFrameworkVersion");
                    TargetFramework = new FrameworkName(string.Format(".NETFramework,Version={0}", framework));

                    // find all project references
                    foreach (XmlElement reference in xml.SelectNodes(string.Format("//{0}:Project/{0}:ItemGroup/{0}:ProjectReference", NamespacePrefix), nsmgr))
                    {
                        // Get the include attribute, and if it's available, load the nested project file
                        var include = reference.Attributes["Include"];
                        if (include != null)
                        {
                            var pathToProject = Path.Combine(Path.GetDirectoryName(ProjectPath), include.Value);
                            try
                            {
                                References.Add(new ProjectFile(new FileInfoWrapper(new FileInfo(pathToProject))));
                            }
                            catch (InvalidOperationException e)
                            {
                                AddWarning(string.Format("Referenced project {0} could not be loaded.", include.Value), e);
                            }
                        }
                    }

                    var packagesConfigPath = xml.SelectSingleNode(string.Format("//{0}:Project/{0}:ItemGroup/{0}:None/@Include", NamespacePrefix), nsmgr);
                    if (packagesConfigPath != null)
                    {
                        PackagesConfigFile = new FileInfoWrapper(new FileInfo(packagesConfigPath.Value));
                    }
                }
            }
            catch (XmlException e)
            {
                throw new InvalidProjectFileException(projectFile.FullName, null, e);
            }
        }

        public string ProjectPath { get; private set; }
        public string ProjectName { get; private set; }
        public ICollection<IProjectFile> References { get; private set; }
        public FrameworkName TargetFramework { get; private set; }
        public FileInfoBase PackagesConfigFile { get; private set; }

        private string GetNodeValueFromPropertyGroup(XmlDocument xml, XmlNamespaceManager nsmgr, string elementName)
        {
            var element = xml.SelectSingleNode(string.Format("//{0}:Project/{0}:PropertyGroup/{0}:{1}", NamespacePrefix, elementName), nsmgr);
            if (element == null) throw new InvalidProjectFileException(ProjectPath, elementName);

            return element.InnerText;
        }

        private void AddWarning(string warning, Exception e = null)
        {
        }
    }
}
