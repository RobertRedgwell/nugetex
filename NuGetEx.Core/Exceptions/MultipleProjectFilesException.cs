﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuGetEx.Core.Exceptions
{
    public class MultipleProjectFilesException : InvalidOperationException
    {
        public MultipleProjectFilesException(string folder)
            : base(string.Format("More than one project (csproj) file found in the folder {0}.", folder))
        {
        }
    }
}
