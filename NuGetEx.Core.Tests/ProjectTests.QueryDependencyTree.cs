﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NuGet;
using NuGetEx.Core.Exceptions;
using NUnit.Framework;

namespace NuGetEx.Core.Tests
{
    [TestFixture]
    public partial class ProjectTests
    {
        private static readonly FrameworkName FrameworkName = new FrameworkName(".NETFramework,Version=v4.5");
        private const string ProjectName = "My.Project";
        private const string ProjectFileName = ProjectName + ".csproj";
        private const string FirstPackageId = "First.Package";
        private const string FirstPackageVersion = "2.0.1";
        private const string SecondPackageId = "Second.Package";
        private const string SecondPackageVersion = "1.4.2-prerelease";
        private const string ValidPackagesConfig = @"<?xml version=""1.0"" encoding=""utf-8""?>
<packages>
    <package id=""First.Package"" version=""2.0.1"" targetFramework=""net45"" />
    <package id=""Second.Package"" version=""1.4.2-prerelease"" targetFramework=""net45"" />
</packages>";
        
        [Test]
        public void QueryDependencyTree_GivenNullPackagesConfigFile_ReturnsValidPackageDescriptor()
        {
            var rootDescriptor = new TestProject(GetMockProjectFile(null))
                .QueryDependencyTree();

            Assert.IsNotNull(rootDescriptor);
            Assert.AreEqual(ProjectName, rootDescriptor.Id);
            Assert.AreEqual("0.0", rootDescriptor.Version.ToString());
            Assert.AreEqual(0, rootDescriptor.Dependencies.Count);
        }

        [Test]
        public void QueryDependencyTree_GivenValidPackagesConfigFile_QueriesPackageRepositoryForPackageTree()
        {
            var project = GetMockProjectFile(ValidPackagesConfig);
            var packageNames = new List<IPackageName>();
            var packageDescriptors = new List<PackageDescriptor>
            {
                new PackageDescriptor { Id = Guid.NewGuid().ToString() },
                new PackageDescriptor { Id = Guid.NewGuid().ToString(), Dependencies = new List<IPackageName> { new Mock<IPackageName>().Object } }
            };
            var resolver = new Mock<IPackageTreeResolver>();
            resolver.Setup(x => x.PackagesFromConfig(project.PackagesConfigFile))
                .Returns(packageNames)
                .Verifiable();
            resolver.Setup(x => x.Resolve(packageNames))
                .Returns(packageDescriptors)
                .Verifiable();

            var rootDescriptor = new TestProject(project, resolver.Object)
                .QueryDependencyTree();

            resolver.Verify();

            Assert.IsNotNull(rootDescriptor);
            Assert.AreEqual(ProjectName, rootDescriptor.Id);
            Assert.AreEqual("0.0", rootDescriptor.Version.ToString());
            Assert.AreEqual(packageDescriptors.Count, rootDescriptor.Dependencies.Count);
            Assert.IsNotNull(rootDescriptor.Dependencies.FirstOrDefault(x => x.Id == packageDescriptors[0].Id));
            Assert.IsNotNull(rootDescriptor.Dependencies.FirstOrDefault(x => x.Id == packageDescriptors[1].Id));
        }

        [Test]
        public void QueryDependencyTree_GivenProjectWithReferences_QueriesPackageRepositoryForEachDependentProject()
        {
            var project = GetMockProjectFile(ValidPackagesConfig);
            var childProject = GetMockProjectFile(ValidPackagesConfig);
            project.References.Add(childProject);
            var parentPackageNames = new List<IPackageName> { new Mock<IPackageName>().Object };
            var childPackageNames = new List<IPackageName> { new Mock<IPackageName>().Object };
            var totalResolvablePackages = parentPackageNames.Count + childPackageNames.Count;
            var packageDescriptors = new List<PackageDescriptor>
            {
                new PackageDescriptor { Id = Guid.NewGuid().ToString() },
                new PackageDescriptor { Id = Guid.NewGuid().ToString(), Dependencies = new List<IPackageName> { new Mock<IPackageName>().Object } }
            };
            var resolver = new Mock<IPackageTreeResolver>();
            resolver.Setup(x => x.PackagesFromConfig(project.PackagesConfigFile))
                .Returns(parentPackageNames)
                .Verifiable();
            resolver.Setup(x => x.PackagesFromConfig(childProject.PackagesConfigFile))
                .Returns(childPackageNames)
                .Verifiable();
            resolver.Setup(x => x.Resolve(It.Is<ICollection<IPackageName>>(c => c.Count == totalResolvablePackages)))
                .Returns(packageDescriptors)
                .Verifiable();
            
            var rootDescriptor = new TestProject(project, resolver.Object)
                .QueryDependencyTree();

            resolver.Verify();

            Assert.IsNotNull(rootDescriptor);
            Assert.AreEqual(ProjectName, rootDescriptor.Id);
            Assert.AreEqual("0.0", rootDescriptor.Version.ToString());
            Assert.AreEqual(packageDescriptors.Count, rootDescriptor.Dependencies.Count);
            Assert.IsNotNull(rootDescriptor.Dependencies.FirstOrDefault(x => x.Id == packageDescriptors[0].Id));
            Assert.IsNotNull(rootDescriptor.Dependencies.FirstOrDefault(x => x.Id == packageDescriptors[1].Id));
        }

        private IPackage GetMockPackageWithDependencies(string packageId, string packageVersion, List<PackageDependency> dependencies)
        {
            var package = new Mock<IPackage>();
            package.SetupGet(x => x.DependencySets)
                .Returns(new List<PackageDependencySet> { new PackageDependencySet(FrameworkName, dependencies) });
            package.SetupGet(x => x.Id)
                .Returns(packageId);
            package.SetupGet(x => x.Version)
                .Returns(new SemanticVersion(packageVersion));

            return package.Object;
        }

    }
}
