﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NuGet;
using NuGetEx.Core.Exceptions;
using NUnit.Framework;

namespace NuGetEx.Core.Tests
{
    [TestFixture]
    public partial class PackageTreeResolverTests
    {
        [Test]
        public void PackagesFromConfig_GivenNullParamter_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new TestPackageTreeResolver().PackagesFromConfig(null));
        }

        [Test]
        [TestCase("")]
        [TestCase("badly formatted")]
        public void PackagesFromConfig_GivenBadlyFormattedPackagesConfigFile_ThrowsInvalidPackageFileException(string packagesFileContents)
        {
            var mockPackagesConfig = GetMockPackageConfigFile(packagesFileContents);
            Assert.Throws<InvalidPackageFileException>(
                () => new TestPackageTreeResolver().PackagesFromConfig(mockPackagesConfig));
        }

        [Test]
        [TestCase("<?xml version=\"1.0\" encoding=\"utf-8\"?><unexpectedRootElement></unexpectedRootElement>")]
        [TestCase("<?xml version=\"1.0\" encoding=\"utf-8\"?><packages><package version=\"1.2.3\" /></packages>")]
        [TestCase("<?xml version=\"1.0\" encoding=\"utf-8\"?><packages><package id=\"My.Test.Package\" /></packages>")]
        [TestCase("<?xml version=\"1.0\" encoding=\"utf-8\"?><packages><package id=\"My.Test.Package\" version=\"NotAValidVersion\" /></packages>")]
        public void QueryDependencyTree_GivenMissingOrInvalidAttributes_OmitsPackageFromResult(string packageConfig)
        {
            var result = new TestPackageTreeResolver()
                .PackagesFromConfig(GetMockPackageConfigFile(packageConfig));

            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count);
        }

        [Test]
        public void QueryDependencyTree_GivenValidPackagesConfig_ReturnsExpectedResult()
        {
            var package1Id = "My.First.Package";
            var package1Version = "4.2.73";
            var package2Id = "My.Second.Package";
            var package2Version = "1.4.2-prerelease";
            var config = string.Format(
                "<?xml version=\"1.0\" encoding=\"utf-8\"?><packages><package id=\"{0}\" version=\"{1}\" /><package id=\"{2}\" version=\"{3}\" /></packages>", 
                package1Id, package1Version, package2Id, package2Version);

            var packageRepo = new Mock<IPackageLookup>();
            var result = new TestPackageTreeResolver()
                .PackagesFromConfig(GetMockPackageConfigFile(config));

            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.AreEqual(package1Id, result.First().Id);
            Assert.AreEqual(package1Version, result.First().Version.ToString());
            Assert.AreEqual(package2Id, result.Last().Id);
            Assert.AreEqual(package2Version, result.Last().Version.ToString());
        }
    }
}
