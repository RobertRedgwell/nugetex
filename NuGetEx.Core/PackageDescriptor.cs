﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuGet;

namespace NuGetEx.Core
{
    /// <summary>
    /// A PackageDescriptor represents a package with an Id, a specific version, and zero or more dependencies
    /// </summary>
    public class PackageDescriptor : IPackageName
    {
        public PackageDescriptor()
        {
            Dependencies = new List<IPackageName>();
        }

        public string Id { get; set; }
        public SemanticVersion Version { get; set; }
        public ICollection<IPackageName> Dependencies { get; set; }
    }
}
