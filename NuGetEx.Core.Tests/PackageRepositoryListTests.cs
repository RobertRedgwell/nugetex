﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NuGet;
using NUnit.Framework;

namespace NuGetEx.Core.Tests
{
    [TestFixture]
    public partial class PackageRepositoryListTests
    {
        [Test]
        public void Ctr_GivenNullList_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new PackageRepositoryList(null));
        }

        [Test]
        public void Ctr_GivenEmptyList_ThrowsArgumentException()
        {
            Assert.Throws<ArgumentException>(() => new PackageRepositoryList(new List<IPackageRepository>()));
        }

        private static ICollection<IPackageRepository> GetFakeRepoList()
        {
            return new List<IPackageRepository>
            {
                new Mock<IPackageRepository>().Object
            };
        }
    }
}
