﻿using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuGet;

namespace NuGetEx.Core
{
    public interface IPackageTreeResolver
    {
        /// <summary>
        /// Returns a list of packages from the config file at the specified location
        /// </summary>
        /// <param name="packagesConfigFile"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">thrown if the packagesConfigFile arg is null</exception>
        /// <exception cref="InvalidPackageFileException">thrown if the specified package file is not valid</exception>
        ICollection<IPackageName> PackagesFromConfig(FileInfoBase packagesConfigFile);

        /// <summary>
        /// Retrieves the specified package's dependencies from the configured repository and returns a tree of packages.
        /// </summary>
        /// <param name="packages"></param>
        /// <returns></returns>
        ICollection<PackageDescriptor> Resolve(ICollection<IPackageName> packages);
    }
}
