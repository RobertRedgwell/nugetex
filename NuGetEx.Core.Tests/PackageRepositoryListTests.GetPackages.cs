﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NuGet;
using NUnit.Framework;

namespace NuGetEx.Core.Tests
{
    [TestFixture]
    public partial class PackageRepositoryListTests
    {
        [Test]
        public void GetPackages_GivenPackageInFirstRepo_QueriesReposInCorrectOrderAndReturnsFirstMatchingPackage()
        {
            var repo1 = GetMockRepoContainingPackage("package1");
            var repo2 = GetMockRepoContainingPackage("packageQ");
            var repo3 = GetMockRepoContainingPackage("packageA");

            var repos = new List<IPackageRepository> { repo1.Object, repo2.Object, repo3.Object };

            var packages = new PackageRepositoryList(repos).GetPackages(x => x.Id == "package1");
            repo1.Verify(x => x.GetPackages(), Times.Once);
            repo2.Verify(x => x.GetPackages(), Times.Never);
            repo3.Verify(x => x.GetPackages(), Times.Never);
            Assert.AreEqual(1, packages.Count);
            Assert.AreEqual("package1", packages.First().Id);
        }

        [Test]
        public void GetPackages_GivenPackageInLastRepo_QueriesReposInCorrectOrderAndReturnsFirstMatchingPackage()
        {
            var repo1 = GetMockRepoContainingPackage("package1");
            var repo2 = GetMockRepoContainingPackage("packageQ");
            var repo3 = GetMockRepoContainingPackage("packageA");

            var repos = new List<IPackageRepository> { repo1.Object, repo2.Object, repo3.Object };

            var packages = new PackageRepositoryList(repos).GetPackages(x => x.Id == "packageA");
            repo1.Verify(x => x.GetPackages(), Times.Once);
            repo2.Verify(x => x.GetPackages(), Times.Once);
            repo3.Verify(x => x.GetPackages(), Times.Once);
            Assert.AreEqual(1, packages.Count);
            Assert.AreEqual("packageA", packages.First().Id);
        }

        private static Mock<IPackageRepository> GetMockRepoContainingPackage(string packageId)
        {
            var package = new Mock<IPackage>();
            package.SetupGet(x => x.Id).Returns(packageId);

            var repo = new Mock<IPackageRepository>();
            repo.Setup(x => x.GetPackages())
                .Returns(new List<IPackage> { package.Object }.AsQueryable())
                .Verifiable();

            return repo;
        }
    }
}
