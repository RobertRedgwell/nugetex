﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuGetEx.Core.Exceptions
{
    public class InvalidPackageFileException : InvalidOperationException
    {
        public InvalidPackageFileException(string pathToFile, Exception innerException = null)
            : base(string.Format("The packages.config file at {0} was not correctly formatted", pathToFile), innerException)
        {
        }
    }
}
