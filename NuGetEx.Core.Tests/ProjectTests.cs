﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;

namespace NuGetEx.Core.Tests
{
    [TestFixture]
    public partial class ProjectTests
    {
        [Test]
        public void Ctr_GivenNullArgs_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new Project(null, new Mock<IPackageTreeResolver>().Object));
            Assert.Throws<ArgumentNullException>(() => new Project(GetMockProjectFile(), null));
        }
        
        public class TestProject : Project
        {
            public TestProject(IProjectFile projectFile = null, IPackageTreeResolver packageTreeResolver = null)
                : base(projectFile ?? GetMockProjectFile(), packageTreeResolver ?? new Mock<IPackageTreeResolver>().Object)
            {

            }
        }

        private static IProjectFile GetMockProjectFile(string packagesConfigContents = ValidPackagesConfig)
        {
            var mock = new Mock<IProjectFile>();
            mock.SetupGet(x => x.References).Returns(new List<IProjectFile>());
            mock.SetupGet(x => x.ProjectName).Returns(ProjectName);
            mock.SetupGet(x => x.ProjectPath).Returns(Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString()));
            mock.SetupGet(x => x.TargetFramework).Returns(FrameworkName);
            if (packagesConfigContents != null)
                mock.SetupGet(x => x.PackagesConfigFile).Returns(GetMockPackageConfigFile(packagesConfigContents));

            var childProject = new Mock<IProjectFile>();
            childProject.SetupGet(x => x.References).Returns(new List<IProjectFile>());
            childProject.SetupGet(x => x.ProjectName).Returns(ProjectName + ".Child");
            childProject.SetupGet(x => x.ProjectPath).Returns(Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString()));
            childProject.SetupGet(x => x.TargetFramework).Returns(FrameworkName);

            mock.SetupGet(x => x.References).Returns(new List<IProjectFile> { childProject.Object });

            return mock.Object;
        }

        private static FileInfoBase GetMockPackageConfigFile(string packagesConfigContents)
        {
            var packagesFile = new Mock<FileInfoBase>();
            packagesFile.Setup(x => x.OpenText())
                .Returns(new StreamReader(new MemoryStream(ASCIIEncoding.ASCII.GetBytes(packagesConfigContents))));

            return packagesFile.Object;
        }
    }
}
