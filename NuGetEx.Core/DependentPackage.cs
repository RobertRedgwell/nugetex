﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuGet;

namespace NuGetEx.Core
{
    /// <summary>
    /// A dependent package is a type of package descriptor that is a dependency on another package.
    /// It includes a min and max version range that is specified in the nuspec of the parent package which lists this as a dependency.
    /// </summary>
    public class DependentPackage : PackageDescriptor, IVersionSpec
    {
        public bool IsMaxInclusive { get; private set; }
        public bool IsMinInclusive { get; private set; }
        public SemanticVersion MaxVersion { get; private set; }
        public SemanticVersion MinVersion { get; private set; }
    }
}
