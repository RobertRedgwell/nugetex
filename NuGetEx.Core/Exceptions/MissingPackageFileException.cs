﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuGetEx.Core.Exceptions
{
    public class MissingPackageFileException : InvalidOperationException
    {
        public MissingPackageFileException(string folder)
            : base(string.Format("No packages.config could be found in the folder {0}.", folder))
        {
        }
    }
}
