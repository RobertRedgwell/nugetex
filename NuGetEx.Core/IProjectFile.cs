﻿using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;

namespace NuGetEx.Core
{
    public interface IProjectFile
    {
        string ProjectPath { get; }
        string ProjectName { get; }
        FileInfoBase PackagesConfigFile { get; }
        ICollection<IProjectFile> References { get; }
        FrameworkName TargetFramework { get; }
    }
}
