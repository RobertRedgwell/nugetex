﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;
using System.IO.Abstractions;
using NuGet;
using System.IO;
using System.Xml;
using NuGetEx.Core.Exceptions;

namespace NuGetEx.Core
{
    /// <summary>
    /// Represents a (cs) project which has a set of references and dependencies
    /// </summary>
    public class Project
    {
        public const string PackagesConfigFileName = "packages.config";
        private readonly IProjectFile _projectFile;
        private readonly IPackageTreeResolver _packageTreeResolver;

        public Project(IProjectFile projectFile, IPackageTreeResolver packageTreeResolver)
        {
            if (projectFile == null) throw new ArgumentNullException("projectFile");
            if (packageTreeResolver == null) throw new ArgumentNullException("packageTreeResolver");

            _projectFile = projectFile;
            _packageTreeResolver = packageTreeResolver;
        }

        public PackageDescriptor QueryDependencyTree()
        {
            var rootDescriptor = new PackageDescriptor
            {
                Id = _projectFile.ProjectName,
                Version = new SemanticVersion("0.0")
            };

            if (_projectFile.PackagesConfigFile == null) 
                return rootDescriptor;


            // Get the nuget packages defined in the project's packages.config:
            var nugetPackages = _packageTreeResolver.PackagesFromConfig(_projectFile.PackagesConfigFile);

            // Now we have all of our top level dependencies, we need to add all of our dependent project's packages too:
            var dependentProjects = _projectFile.References.ToList();
            while (dependentProjects.Count > 0)
            {
                // Grab the nuget packages if any
                if (dependentProjects[0].PackagesConfigFile != null)
                    nugetPackages.AddRange(_packageTreeResolver.PackagesFromConfig(dependentProjects[0].PackagesConfigFile));

                // Add the range of dependencies
                dependentProjects.AddRange(dependentProjects[0].References);
                // remove this project from the beginning of the list
                dependentProjects.RemoveAt(0);
            }

            // Now we have a list of all of the various dependencies and their versions. So go off and create a tree of dependencies, including all of the child dependencies of each package:
            var resolvedPackages = _packageTreeResolver.Resolve(nugetPackages);
            if (resolvedPackages != null)
                rootDescriptor.Dependencies.AddRange(resolvedPackages.Cast<IPackageName>());
            

            return rootDescriptor;
        }

        //private List<PackageDescriptor> ReadPackageDescriptors(

        private void AddWarning(string warning) 
        {
        }
    }
}
