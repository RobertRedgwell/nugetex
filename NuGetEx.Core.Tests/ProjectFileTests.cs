﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NuGetEx.Core.Exceptions;
using NUnit.Framework;

namespace NuGetEx.Core.Tests
{
    [TestFixture]
    public class ProjectFileTests
    {
        private const string ProjectFileHeader = "<?xml version=\"1.0\" encoding=\"utf-8\"?><Project ToolsVersion=\"4.0\" DefaultTargets=\"Build\" xmlns=\"http://schemas.microsoft.com/developer/msbuild/2003\">";
        private const string ProjectFileAssemblyNameSection = @"
<PropertyGroup>
    <AssemblyName>ExampleProject</AssemblyName>
    <TargetFrameworkVersion>v4.5</TargetFrameworkVersion>
</PropertyGroup>";

        private const string ProjectFilePackageConfigSection = @"
<ItemGroup>
    <None Include=""packages.config"" />
</ItemGroup>";

        private const string ProjectFileReferenceSection = @"
<ItemGroup>
    <ProjectReference Include=""Child.Project.csproj"">
        <Name>Child.Project</Name>
    </ProjectReference>
</ItemGroup>";
        private const string ProjectFileFooter = "</Project>";

        [Test]
        public void Ctr_GivenNullParameters_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new ProjectFile(null));
        }

        [Test]
        public void Ctr_GivenProjectFileDoesNotExist_ThrowsMissingProjectFileException()
        {
            var mockFile = new Mock<FileInfoBase>();
            mockFile.Setup(x => x.Exists).Returns(false).Verifiable();

            Assert.Throws<MissingProjectFileException>(() => new ProjectFile(mockFile.Object));

            mockFile.Verify();
        }

        [Test]
        [TestCase("")]
        [TestCase(ProjectFileHeader)]
        [TestCase(ProjectFileHeader + ProjectFileFooter)]
        [TestCase(ProjectFileHeader + ProjectFileReferenceSection + ProjectFileFooter)]
        [TestCase(ProjectFileAssemblyNameSection + ProjectFileFooter)]
        [TestCase(ProjectFileAssemblyNameSection + ProjectFileReferenceSection + ProjectFileFooter)]
        [TestCase(ProjectFileReferenceSection + ProjectFileFooter)]
        public void Ctr_GivenProjectFileWithMissingContents_ThrowsInvalidProjectFileException(string contents)
        {
            var mockFile = GetProjectFile(contents);

            Assert.Throws<InvalidProjectFileException>(() => new ProjectFile(mockFile.Object));

            mockFile.Verify();
        }

        [Test]
        public void Ctr_GivenValidProjectFileWithNoReferences_SetsReferencesToEmptyCollection()
        {
            var mockFile = GetProjectFile(ProjectFileHeader + ProjectFileAssemblyNameSection + ProjectFileFooter);

            var projectFile = new ProjectFile(mockFile.Object);
            Assert.IsNotNull(projectFile.References);
            Assert.AreEqual(0, projectFile.References.Count);

            mockFile.Verify();
        }

        [Test]
        public void Ctr_GivenValidProjectFileWithValidSections_SetsPropertiesAsExpected()
        {
            string rootPath = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
            try
            {
                var rootFolder = Directory.CreateDirectory(rootPath);
                var projectFileTop = new FileInfo(Path.Combine(rootFolder.FullName, "My.Project.csproj"));
                var projectFileChild = new FileInfo(Path.Combine(rootFolder.FullName, "Child.Project.csproj"));
                File.WriteAllText(projectFileTop.FullName, ProjectFileHeader + ProjectFileAssemblyNameSection + ProjectFileReferenceSection + ProjectFilePackageConfigSection + ProjectFileFooter);
                File.WriteAllText(projectFileChild.FullName, ProjectFileHeader + ProjectFileAssemblyNameSection + ProjectFileFooter);

                var projectFile = new ProjectFile(projectFileTop);
                Assert.AreEqual(projectFileTop.FullName, projectFile.ProjectPath);
                Assert.AreEqual("ExampleProject", projectFile.ProjectName);
                Assert.IsNotNull(projectFile.TargetFramework);
                Assert.AreEqual(new FrameworkName(".NETFramework,Version=v4.5").FullName, projectFile.TargetFramework.FullName);
                Assert.IsNotNull(projectFile.PackagesConfigFile);
                Assert.AreEqual("packages.config", projectFile.PackagesConfigFile.Name);
                Assert.IsNotNull(projectFile.References);
                Assert.AreEqual(1, projectFile.References.Count);
                Assert.AreEqual("ExampleProject", projectFile.References.First().ProjectName);
                Assert.AreEqual(0, projectFile.References.First().References.Count);
                Assert.IsNull(projectFile.References.First().PackagesConfigFile);
            }
            finally
            {
                Directory.Delete(rootPath, true);
            }
        }

        private static Mock<FileInfoBase> GetProjectFile(string contents)
        {
            var mockFile = new Mock<FileInfoBase>();
            mockFile.Setup(x => x.Exists).Returns(true).Verifiable();
            mockFile
                .Setup(x => x.OpenText())
                .Returns(new StreamReader(new MemoryStream(Encoding.ASCII.GetBytes(contents))))
                .Verifiable();

            mockFile.SetupGet(x => x.FullName).Returns(Path.Combine(Path.GetTempPath(), "project.csproj")).Verifiable();
            return mockFile;
        }

    }
}
