﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NuGet;
using NUnit.Framework;

namespace NuGetEx.Core.Tests
{
    [TestFixture]
    public partial class PackageTreeResolverTests
    {
        [Test]
        public void Ctr_GivenNullLookup_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new PackageTreeResolver(null));
        }

        public class TestPackageTreeResolver : PackageTreeResolver
        {
            public TestPackageTreeResolver(IPackageLookup packageLookup = null)
                : base(packageLookup ?? new Mock<IPackageLookup>().Object)
            {
            }
        }

        private static FileInfoBase GetMockPackageConfigFile(string packagesConfigContents)
        {
            var packagesFile = new Mock<FileInfoBase>();
            packagesFile.Setup(x => x.OpenText())
                .Returns(new StreamReader(new MemoryStream(ASCIIEncoding.ASCII.GetBytes(packagesConfigContents))));

            return packagesFile.Object;
        }
    }
}
