﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NuGet;

namespace NuGetEx.Core
{
    // TODO - use NuGet.PriorityPackageRepository instead...!

    /// <summary>
    /// Implementation of a package repository that queries multiple package repositories.
    /// </summary>
    public class PackageRepositoryList 
    {
        private readonly ICollection<IPackageRepository> _prioritisedRepos;

        public PackageRepositoryList(ICollection<IPackageRepository> prioritisedRepos)
        {
            if (prioritisedRepos == null) throw new ArgumentNullException("prioritisedRepos");
            if (prioritisedRepos.Count == 0) throw new ArgumentException("prioritisedRepos must have at least one entry");

            _prioritisedRepos = prioritisedRepos;
        }

        public ICollection<IPackage> GetPackages(Func<IPackage, bool> predicate)
        {
            foreach (var repo in _prioritisedRepos)
            {
                var packages = repo.GetPackages().Where(predicate).ToList();
                if (packages.Count > 0) return packages;
            }
            return new List<IPackage>();
        }
    }
}
