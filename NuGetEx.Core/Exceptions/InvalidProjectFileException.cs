﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NuGetEx.Core.Exceptions
{
    public class InvalidProjectFileException : InvalidOperationException
    {
        public InvalidProjectFileException(string projectFile, string missingItem = null, Exception innerException = null)
            : base(string.Format("The project file at {0} is invalid. Unable to find {1}.", projectFile, missingItem), innerException)
        {
        }
    }
}
