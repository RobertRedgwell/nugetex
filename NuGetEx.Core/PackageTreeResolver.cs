﻿using System;
using System.Collections.Generic;
using System.IO.Abstractions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using NuGet;
using NuGetEx.Core.Exceptions;

namespace NuGetEx.Core
{
    public class PackageTreeResolver : IPackageTreeResolver
    {
        private readonly IPackageLookup _packageLookup;
        public PackageTreeResolver(IPackageLookup packageLookup)
        {
            if (packageLookup == null) throw new ArgumentNullException("packageLookup");
            _packageLookup = packageLookup;
        }

        public ICollection<IPackageName> PackagesFromConfig(FileInfoBase packagesConfigFile)
        {
            if (packagesConfigFile == null) throw new ArgumentNullException("packagesConfigFile");

            var result = new List<IPackageName>();

            var xml = new XmlDocument();
            using (var reader = packagesConfigFile.OpenText())
            {
                try
                {
                    xml.Load(reader);
                    foreach (XmlNode package in xml.SelectNodes("/packages/package"))
                    {
                        var id = package.Attributes["id"];
                        if (id == null)
                        {
                            AddWarning("Package element has a missing id attribute.");
                            continue;
                        }

                        var version = package.Attributes["version"];
                        if (version == null)
                        {
                            AddWarning(string.Format("Package element for {0} has a missing version attribute.", id.Value));
                            continue;
                        }

                        SemanticVersion semanticVersion;
                        if (!SemanticVersion.TryParse(version.Value, out semanticVersion))
                        {
                            AddWarning(string.Format("Package element for {0} has an invalid semantic version attribute.", id.Value));
                            continue;
                        }

                        result.Add(new PackageName(id.Value, semanticVersion));
                    }
                }
                catch (XmlException e)
                {
                    throw new InvalidPackageFileException(packagesConfigFile.FullName, e);
                }
            }

            return result;
        }

        /// <summary>
        /// Retrieves the specified package's dependencies from the configured repository and returns a tree of packages.
        /// </summary>
        /// <param name="packages"></param>
        /// <returns></returns>
        public ICollection<PackageDescriptor> Resolve(ICollection<IPackageName> packages)
        {
            throw new NotImplementedException();
            //packageRepository.FindPackage("").DependencySets
        }

        private void AddWarning(string message)
        {
        }

    }
}
